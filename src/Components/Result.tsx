import { useEffect, useState } from "react"

interface ResultProps {
    results: ({ q: string; a: string; })[];
    data: {question: string; options: string[]; answer: string}[];
    onReset: () => void;
    onAnswersCheck: () => void;
    time: number;
}
const Result: React.FC<ResultProps> = ({results, data, onReset, onAnswersCheck, time}) => {

    const [correctAnswers, setCorrectAnswers] = useState(0);
    const [remarks, setRemarks] = useState('');
    useEffect(() => {
        let correct = 0;
        results.forEach((result, index) => {
            if(result.a === data[index].answer) {
                correct++;
            }
        });
        setCorrectAnswers(correct);
        if(correctAnswers >= 10) {
            setRemarks('Bravo! Test passed :)')
        }
        else{
            setRemarks('Test failed... :(');
        }
    });
    return(
        <div className="card">
            <div className="card-content">
                <div className="content">
                    <h3>{remarks}</h3>
                    <p>{correctAnswers} out of {data.length}</p>
                    <p><strong>{Math.floor((correctAnswers / data.length) * 100)}%</strong></p>
                    <p><strong>Time taken: </strong>{time} s</p>
                    <button className="button is-info mr-2" onClick={onAnswersCheck}>Detailed Analysis</button>
                    <button className="button is-success" onClick={onReset}>Take quiz again</button>
                </div>
            </div>
        </div>
    )
}


export default Result;