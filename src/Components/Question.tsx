import React, {useState, useEffect, useRef} from 'react';

interface QuestionProps {
    data: {question: string; options: string[]; answer: string};
    onAnswerUpdate: React.Dispatch<React.SetStateAction<({ q: string; a: string; })[]>>;
    numberOfQuestions: number;
    activeQuestion: number;
    onSetActiveQuestion:  React.Dispatch<React.SetStateAction<number>>;
    onSetStep: React.Dispatch<React.SetStateAction<number>>;
}
const Question: React.FC<QuestionProps> = ({data, onAnswerUpdate, numberOfQuestions, activeQuestion, onSetActiveQuestion, onSetStep}) => {

    const [selected, setSelected] = useState('');
    const [error, setError] = useState('');
    const radiosWrapper = React.useRef() as React.MutableRefObject<HTMLInputElement>;

    useEffect(() => {
        const findCheckedInput = radiosWrapper.current.querySelector('input:checked') as HTMLInputElement;
        if(findCheckedInput){
            findCheckedInput.checked=false;
        }
    }, [data]);

    const onChangeHandler = (e: React.ChangeEvent<HTMLInputElement>) => {
        setSelected(e.target.value);
        if(error){
            setError('');
        }
    }

    const nextClickHandler = (e: React.MouseEvent<HTMLButtonElement, MouseEvent>) => {
        if(selected === '') {
            return setError("Please select an option!");
        }
        onAnswerUpdate(prevState => [...prevState, {q: data.question, a: selected}]);
        setSelected('');
        if(activeQuestion < numberOfQuestions - 1){
            onSetActiveQuestion(activeQuestion+1);
        }
        else{
            onSetStep(3);
        }
    }
    return(
        <div className="card">
            <div className="card-content">
                <div className="content">
                    <h3 className="mb-6">
                        {data.question}
                    </h3>
                    <div className="control" ref={radiosWrapper}>
                        {data.options.map((option, i) => (
                            <label className="radio has-background-light" key={i}>
                            <input type="radio" name="answer" value={option} onChange={onChangeHandler}/>
                            {option}
                            </label>
                        ))}
                    </div>
                    {error && <div className="has-text-danger">
                        {error} 
                    </div>}
                    <button className="button is-link is-medium is-fullwidth mt-4" onClick={nextClickHandler}>Next</button>
                </div>
            </div>
        </div>
    )
}

export default Question;