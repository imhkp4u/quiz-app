import React from 'react';

interface ModalProps {
    onClose: any;
    results: ({ q: string; a: string; })[];
    data: {question: string; options: string[]; answer: string}[];
}
const Modal: React.FC<ModalProps> = ({onClose, results, data}) => {
    return (
        <div className="modal is-active">
            <div className="modal-background" onClick={onClose}></div>
            <div className="modal-card">
                <header className="modal-card-head">
                    <p className="modal-card-title">Detailed Analysis</p>
                    <button className="delete" onClick={onClose}></button>
                </header>
                <section className="modal-card-body content">
                    <ol>
                        {results.map((result, i) => (
                            <li key={i} className="mb-6">
                                <p><strong>{result.q}</strong></p>
                                <p className={result.a === data[i].answer ? 'has-background-success hast-text-white p-2' : 
                                'has-background-danger has-text-white p-2'}>You selected :- {result.a}</p>
                                {result.a !== data[i].answer && <p className='has-background-link has-text-white p-2'>
                                    Correct answer :- {data[i].answer}</p>}
                            </li>
                        ))}
                    </ol>
                </section>
            </div>
        </div>
    )
}

export default Modal;