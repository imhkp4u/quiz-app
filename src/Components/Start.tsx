import { start } from "repl";
import React from 'react';

interface StartProps{
    onQuizStart: () => void;
}

const Start: React.FC<StartProps> = ({ onQuizStart }) => {
    return(
        <div className="card">
            <div className="card-content">
                <div className="content">
                    <h1>Start the quiz</h1>
                    <p>Good luck!</p>
                    <button className="button is-info is-medium" onClick={onQuizStart}>Start</button>
                </div>
            </div>
        </div>
    )
}

export default Start;