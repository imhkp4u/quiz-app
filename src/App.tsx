import './App.css';
import Question from './Components/Question';
import Start from './Components/Start';
import { useEffect, useState } from 'react';
// import quizData from './data/question.json';
import Result from './Components/Result';
import Modal from './Components/Modal';

let interval: any;

const App: React.FC = () => {

  const [step, setStep] = useState(1);
  const [activeQuestion, setActiveQuestion] = useState(0);
  const [answers, setAnswers] = useState<({q: string; a: string;})[]>([]);
  const [time, setTime] = useState(0);
  const [showModal, setShowModal] = useState(false);
  const [question, setQuestion] = useState<{question: string; options: string[]; answer: string}[]>([]);

  useEffect(() => {
    if(step === 3){
      clearInterval(interval);
    }
  }, [step]);
  const quizStartHandler = () => {
    setStep(2);
    interval = setInterval(() => {
      setTime(prevTime => prevTime+1);
    }, 1000);
  }

  const resetClickHandler = () => {
    setActiveQuestion(0);
    setAnswers([]);
    setStep(2);
    setTime(0);
    interval = setInterval(() => {
      setTime(prevTime => prevTime+1);
    }, 1000)
  }

  const getQues = async () => {
    let r = await fetch('https://heczmm1cci.execute-api.ap-south-1.amazonaws.com/Dev');
    let question = await r.json();
    setQuestion(question);
  }

  useEffect(() => {
      getQues();
  }, []);

  // console.log(question);

    

  return (
    <div className="App">
      {step === 1 && <Start onQuizStart={quizStartHandler}/>}
      {step === 2 && <Question
        data = {question[activeQuestion]}
        onAnswerUpdate = {setAnswers}
        numberOfQuestions = {question.length}
        activeQuestion = {activeQuestion}
        onSetActiveQuestion = {setActiveQuestion}
        onSetStep = {setStep}
       />}
       {step === 3 && <Result 
       results={answers}
       data={question}
       onReset={resetClickHandler}
       onAnswersCheck={() => setShowModal(true)}
       time={time}
       />}

       {showModal && <Modal
       onClose = {() => setShowModal(false)}
       results={answers}
       data={question} 
       />}
    </div>
  );
}

export default App;
